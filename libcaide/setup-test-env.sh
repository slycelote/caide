#!/bin/bash

# Setup environment variables required for running tests.
# See INSTALL.txt for details.

# Change these paths according to your system.
export PATH="$PATH:/c/ProgramData/chocolatey/lib/ghc/tools/ghc-8.10.1/mingw/bin"
export CSC=/c/Windows/Microsoft.NET/Framework/v4.0.30319/csc.exe
export PHANTOMJS=/c/bin/phantomjs.exe

